/*
	DATABASE OPTICA_CALERO
	SCHEDULED AND PREPARED BY:

	1. ALEJANDRO ANTONIO TELLEZ BARREDA
	2. MELVIN ANTONIO FLORES
	3. PAULINO JOSE ZELAYA TORU�O

	DATABASE PROJECT I
	NATIONAL UNIVERSITY OF ENGINEERING

	..:: ALL THE PROGRAMMING ARE ONLY AND EXCLUSIVELY
	PREPARED BY THE ABOVE MENTIONED PEOPLE ::..

*/

/*
				>>>>>>>>>>	UPDATE STORED PROCEDURES <<<<<<<<<<<<<
*/
--=======================================================================================--
--====================PROCEDURE UPDATE FOR TABLE USERREGISTRATION========================--
DROP PROCEDURE IF EXISTS UserUpdate
GO

CREATE PROCEDURE UserUpdate
	@ID_User TINYINT,
	@ID_Ced VARCHAR(20),
	@User VARCHAR(20),
	@Name VARCHAR(20),
	@LastName VARCHAR(20),
	@Phone VARCHAR(20),
	@Mail VARCHAR(50),
	@Departament VARCHAR(20),
	@Municipality VARCHAR(20),
	@Role VARCHAR(20),
	@Direction VARCHAR(50),
	@RETURN TINYINT OUTPUT,
	@RETURN_MESSAGE VARCHAR(500) OUTPUT
AS
	-------------------------------------------
		DECLARE @ID_Person TINYINT
		SET @ID_Person = (SELECT ID_Person FROM dbo.Users WHERE ID_User = @ID_User)
	-------------------------------------------
	IF EXISTS (SELECT * FROM dbo.ViewUsers WHERE [User] = @User AND ID_User != @ID_User)
		BEGIN
			SET @RETURN = 1;
			SET @RETURN_MESSAGE = 'User Already Exists'			
		END
		ELSE
		BEGIN
			UPDATE dbo.Users
				SET [User] = @User,					
					[Department] = @Departament,
					[Municipality] = @Municipality,
					[Role] = @Role					
					WHERE ID_User = @ID_User
		-------------------------------------------
			UPDATE dbo.Person
				SET [Name] = @Name,
					[LastName] = @LastName,
					[Phone] = @Phone,
					[Mail] = @Mail,
					[Direction] = @Direction
					WHERE ID_Person = @ID_Person
		-------------------------------------------
			SET @RETURN = 0;
			SET @RETURN_MESSAGE = 'User Modified Correctly'
		END
GO
DECLARE @n int,@nm varchar(50)
EXEC UserUpdate '1','001-290698-0015','admin','Paulino','Zelaya','87056446','apaulinojozzzz@gmail.com','Managua','Managua','Admin','Las Torres',@n output,@nm output
SELECT @n Codigo,@nm Mensaje

SELECT * FROM ViewUsers
--=======================================================================================--
--=======================CHANGE PASSWORD USER============================================--
DROP PROCEDURE IF EXISTS ChangePassword
GO

CREATE PROCEDURE ChangePassword
	@OldPassword VARCHAR(100),
	@NewPassword VARCHAR(100),
	@Return INT OUTPUT,
	@Return_Message VARCHAR(500) OUTPUT
AS
	DECLARE @User VARCHAR(500)
	SET @User = (SELECT [User] FROM dbo.Users WHERE ([Password] = HASHBYTES('SHA2_256',@OldPassword)))
	IF EXISTS (SELECT * FROM dbo.Users WHERE [User] = @User AND ([Password] = HASHBYTES('SHA2_256',@OldPassword)))
	BEGIN
		UPDATE dbo.Users
		SET [Password] = HASHBYTES('SHA2_256',@NewPassword)
		WHERE [Password] = HASHBYTES('SHA2_256',@OldPassword)
		SET @Return = 0
		SET @Return_Message = 'Upate Password Succefully'
	END
	ELSE
	BEGIN
		SET @Return = 1
		SET @Return_Message = 'Old Password Is Incorrectly'
	END	
GO

DECLARE @n INT,@nm VARCHAR(500)
EXEC ChangePassword '1234','12345',@n output,@nm output
SELECT @n,@nm
--=======================================================================================--
--=======================CHANGE NEW PASSWORD USER============================================--
DROP PROCEDURE IF EXISTS NewPassword
GO

CREATE PROCEDURE NewPassword
	@Code INT,
	@NewPassword VARCHAR(100),
	@Return INT OUTPUT,
	@Return_Message VARCHAR(100) OUTPUT
AS
	IF EXISTS (SELECT * FROM dbo.Users WHERE RecoveryCode = @Code)
	BEGIN
		UPDATE dbo.Users
		SET [Password] = HASHBYTES('SHA2_256',@NewPassword)
		WHERE [RecoveryCode] = @Code

		UPDATE dbo.Users
		SET [RecoveryCode] = (SELECT ROUND(RAND()*100000,0))
		WHERE [RecoveryCode] = @Code

		SET @Return = 0
		SET @Return_Message = 'Password Change Succefully'
	END
	ELSE
	BEGIN
		SET @Return = 1
		SET @Return_Message = 'Recovery Code Is Incorrectly'
	END
GO

--=======================PROCEDURE UPDATE FOR TABLE CLIENT===============================--
DROP PROCEDURE IF EXISTS ClientUpdate
GO

CREATE PROCEDURE ClientUpdate
	@ID_User VARCHAR(20),
	@ID_Ced VARCHAR(20),
	@Name VARCHAR(20),
	@LastName VARCHAR(20),
	@Age VARCHAR(20),
	@Direction VARCHAR(50),
	@Mail VARCHAR(50),
	@Phone VARCHAR(20),
	@RETURN TINYINT OUTPUT,
	@RETURN_MESSAGE VARCHAR(500) OUTPUT
AS
	-------------------------------------------
		DECLARE @ID_Person TINYINT
		SET @ID_Person = (SELECT ID_Person FROM dbo.Person WHERE Ced = @ID_Ced)
	-------------------------------------------
	IF NOT EXISTS (SELECT * FROM dbo.ViewClients WHERE [Ced] = @ID_Ced AND ID_Client != @ID_User)
		BEGIN
			UPDATE dbo.Client
				SET [Ced] = @ID_Ced,
					[Age] = @Age
					WHERE ID_Client = @ID_User
		-------------------------------------------
			UPDATE dbo.Person
					SET [Name] = @Name,
					[LastName] = @LastName,
					[Phone] = @Phone,
					[Mail] = @Mail,
					[Direction] = @Direction
					WHERE ID_Person = @ID_Person
		-------------------------------------------
			SET @RETURN = 0;
			SET @RETURN_MESSAGE = 'User Modified Correctly'
		END
		ELSE
		BEGIN
			SET @RETURN = 1;
			SET @RETURN_MESSAGE = 'User Already in database'
		END
GO
--=======================================================================================--
--==============================PROCEDURE UPDATE INVENTORY===============================--
DROP PROCEDURE IF EXISTS InventoryUpdate
GO

CREATE PROCEDURE InventoryUpdate
	@ID TINYINT,
	@Code VARCHAR(20),
	@Brand VARCHAR(20),
	@Color VARCHAR(20),
	@Quantity VARCHAR(20),
	@Price VARCHAR(20),
	@Preview IMAGE NULL,
	@RETURN TINYINT OUTPUT,
	@RETURN_MESSAGE VARCHAR(500) OUTPUT
AS
	-----------------------------------------------
     UPDATE dbo.Inventory 
		SET	[Code]=@Code,
			[Brand]=@Brand,
			[Color]=@Color,
			[Quantity]=@Quantity,
			[Price]=@Price,
			[Preview]=@Preview
			WHERE [ID] = @ID
	-----------------------------------------------
		SET @RETURN = 0;
		SET @RETURN_MESSAGE = 'User Modified Correctly'
GO
--=======================================================================================--
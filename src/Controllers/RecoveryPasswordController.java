package Controllers;

import ConnectionSql.ConnectionSQLServer;
import Models.RecoveryPasswordModel;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;
import Misc.getData;
import View.RecoveryPasswordChange;
import View.RecoveryPassword;

public class RecoveryPasswordController implements ActionListener {

    private RecoveryPassword rp;
    private RecoveryPasswordChange rpc;
    private RecoveryPasswordModel rpm;

    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();

    

    public RecoveryPasswordController(RecoveryPasswordChange rpc) {
        super();
        this.rpc = rpc;
        rpm = new RecoveryPasswordModel();
    }

    public RecoveryPasswordController(RecoveryPassword rp) {
         super();
        this.rp = rp;
        rpm = new RecoveryPasswordModel();
    }



    public RecoveryPasswordModel getRpm() {
        return rpm;
    }

    public void setRpm(RecoveryPasswordModel rpm) {
        this.rpm = rpm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Change":
                envio();
                break;

            case "exit":
                rp.dispose();
                break;

            case "New":
                newpassword();
                Enviar();
                break;
                
            case "exitchange":
                rpc.dispose();
                break;
        }
    }

    public void envio() {
        rpm = rp.getData();

        String mail = rpm.getMail();
        try {
            String sql = "SELECT * FROM ViewUsers WHERE Mail = '" + mail + "'";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                envioMail();
                rp.dispose();
                RecoveryPasswordChange r = new RecoveryPasswordChange();
                r.setVisible(true);
            } else {
                rp.errorLabel.setText("Mail not exists");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void envioMail() {
        rpm = rp.getData();

        String mailuser = rpm.getMail();

        try {
            String sql = "SELECT * FROM dbo.ViewUsers WHERE Mail = '" + mailuser + "'";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                String RecoveryCode = rs.getString("RecoveryCode");

                Properties propiedad = new Properties();
                propiedad.setProperty("mail.smtp.host", "smtp.gmail.com");
                propiedad.setProperty("mail.smtp.starttls.enable", "true");
                propiedad.setProperty("mail.smtp.port", "587");
                propiedad.setProperty("mail.smtp.auth", "true");

                Session sesion = Session.getDefaultInstance(propiedad);
                String correoEnvia = "opticascalero@gmail.com";
                String contraseña = "123Putito123";
                String destinatario = rs.getString("Mail");
                String Asunto = "Change of password";

                MimeMessage mail = new MimeMessage(sesion);
                try {
                    mail.setFrom(new InternetAddress(correoEnvia));
                    mail.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
                    mail.setSubject(Asunto);
                    mail.setText("A password change has been requested.\n"
                            + "\n"
                            + "Please enter the following code to restore: " + RecoveryCode + "\n"
                            + "\n"
                            + "If you have not requested a password change, please contact: opticascalero@gmail.com");

                    Transport transportar = sesion.getTransport("smtp");
                    transportar.connect(correoEnvia, contraseña);
                    transportar.sendMessage(mail, mail.getRecipients(Message.RecipientType.TO));
                    transportar.close();

                    //JOptionPane.showMessageDialog(null, "Please check email and whatsapp ", "Notification", JOptionPane.INFORMATION_MESSAGE);
                } catch (AddressException ex) {
                    Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
                    System.out.println(ex.getMessage());
                } catch (MessagingException ex) {
                    Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
                    System.out.println(ex.getMessage());
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void newpassword() {
        rpm = rpc.getData();

        String code = rpm.getRecoverycode();
        String newpassword = rpm.getNewpassword();
        String repeat = rpm.getRepeatpassword();
        
        try {
            String sql = "DECLARE @n INT, @nm VARCHAR(100);"
                    + "EXEC NewPassword '" + code + "','" + newpassword + "',@n OUTPUT,@nm OUTPUT;"
                    + "SELECT @n Codigo,@nm Mensaje";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
            if(code.isEmpty()||newpassword.isEmpty()||repeat.isEmpty()){
                rpc.errorLabel.setText("Fill in all fields");
            }else if(!newpassword.equals(repeat)){
                rpc.errorLabel.setText("Passwords do not match");
            }else if(rs.next()){
                String rotulo = rs.getString("Codigo");
                
                if(rotulo.equals("0")){
                    rpc.errorLabel.setText(rs.getString("Mensaje"));
                }else{
                    rpc.errorLabel.setText(rs.getString("Mensaje"));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void Enviar() {
        try {
            rpm = rpc.getData();
            getData gd = new getData();
            String newp = rpm.getNewpassword();

            String SQL1 = "SELECT * FROM dbo.ViewUsers WHERE [Password] = HASHBYTES('SHA2_256','" + newp + "')";
            Statement stmt = con.createStatement();
            ResultSet rs1 = stmt.executeQuery(SQL1);

            while (rs1.next()) {
                Properties propiedad = new Properties();
                propiedad.setProperty("mail.smtp.host", "smtp.gmail.com");
                propiedad.setProperty("mail.smtp.starttls.enable", "true");
                propiedad.setProperty("mail.smtp.port", "587");
                propiedad.setProperty("mail.smtp.auth", "true");

                Session sesion = Session.getDefaultInstance(propiedad);
                String correoEnvia = "opticascalero@gmail.com";
                String contraseña = "123Putito123";
                String destinatario = rs1.getString("Mail");
                String Asunto = "Change of password";

                MimeMessage mail = new MimeMessage(sesion);
                try {
                    mail.setFrom(new InternetAddress(correoEnvia));
                    mail.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
                    mail.setSubject(Asunto);
                    mail.setText("Dear "+ rs1.getString("Name") + " " + rs1.getString("LastName") + ", you are hereby informed that the password has been changed successfully.\n"
                            + "\n"
                            + "If you have not made the change please write to us at: opticascalero@gmail.com\n"
                            + "\n"
                            + "If you made the change, ignore this email.\n"
                            + "\n"
                            + "Excellent day, Optica Calero.\n\n\n"
                            + "------------------------------------------------\n"
                            + "Additional data:\n"
                            + "\nHost Name: "+ gd.Obtener_Nombre()
                            + "\nPublic IP: "+ gd.ObtenerIP_Publica()
                            + "\nLocal IP: "+gd.ObtenerIP_Local()
                            + "\nMore Information: https://whatismyipaddress.com/ip/"+gd.ObtenerIP_Publica()
                            + "\n------------------------------------------------");

                    Transport transportar = sesion.getTransport("smtp");
                    transportar.connect(correoEnvia, contraseña);
                    transportar.sendMessage(mail, mail.getRecipients(Message.RecipientType.TO));
                    transportar.close();

                    //JOptionPane.showMessageDialog(null, "Please check email and whatsapp ", "Notification", JOptionPane.INFORMATION_MESSAGE);

                } catch (AddressException ex) {
                    Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
                    System.out.println(ex.getMessage());
                } catch (MessagingException ex) {
                    Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
                     System.out.println(ex.getMessage());
                }
            }

            rs1.close();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
             System.out.println(e.getMessage());
        }
    }
}

package Controllers;

import ConnectionSql.ConnectionSQLServer;
import Misc.ShowTables;
import Models.ClientModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import paneles.clientPanel;
import java.sql.*;

public class ClientController implements ActionListener {

    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();

    ShowTables STB = new ShowTables();
    clientPanel CP;
    ClientModel CM;

    public ClientController(clientPanel CP) {
        super();
        this.CP = CP;
        CM = new ClientModel();
        //CP.ErrorLabel.setVisible(false);
        STB.LoadClientTable(CP.ClientTable);
    }

    public clientPanel getCP() {
        return CP;
    }

    public void setCP(clientPanel CP) {
        this.CP = CP;
    }

    public ClientModel getCM() {
        return CM;
    }

    public void setCM(ClientModel CM) {
        this.CM = CM;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "AddClient":
                AddClient();

                break;
            case "ModifyClient":
                ModifyClient();
                break;
            case "DeleteClient":
                DeleteClient();
                break;
            case "Clean":
                CP.Clean();
                break;
        }
    }

    public void AddClient() {
        CM = CP.getClientData();

        try {
            String ID = CM.getID();
            String Ced = CM.getCed();
            String Name = CM.getName();
            String LastName = CM.getLastName();
            String Age = CM.getAge();
            String Direction = CM.getDirection();
            String Mail = CM.getMail();
            String Phone = CM.getPhone();

            Pattern pattern = Pattern.compile("([a-z0-9]+(\\.?[a-z0-9])*)+@(([a-z]+)\\.([a-z]+))+");
            String email = Mail;
            Matcher mather = pattern.matcher(email);

            if (Ced.length() == 14) {
                if (!Character.isLetter(Ced.charAt(13))) {
                    CP.ErrorLabel.setVisible(true);
                    CP.ErrorLabel.setText("Formato de cedula incorrecto");
                } else {
                    if (Name.isEmpty()) {
                        CP.ErrorLabel.setVisible(true);
                        CP.ErrorLabel.setText("Debe Rellenar Todos Los Campos");
                    } else {
                        if (mather.find() == true) {

                            CallableStatement cs = con.prepareCall("{call ClientInsert (?,?,?,?,?,?,?)}");

                            cs.setString(1, CM.getCed());
                            cs.setString(2, CM.getName());
                            cs.setString(3, CM.getLastName());
                            cs.setString(4, CM.getAge());
                            cs.setString(5, CM.getDirection());
                            cs.setString(6, CM.getMail());
                            cs.setString(7, CM.getPhone());

                            ResultSet rs = cs.executeQuery();

                            if (rs.next()) {
                                STB.LoadClientTable(CP.ClientTable);
                                //tiempo.start();
                                CP.ErrorLabel.setVisible(true);
                                CP.ErrorLabel.setText(rs.getString(1));
                            }
                            //Enviar();

                        } else {
                            CP.ErrorLabel.setVisible(true);
                            CP.ErrorLabel.setText("El Mail ingresado debe ser valido");
                        }
                    }
                }

            } else {
                CP.ErrorLabel.setVisible(true);
                CP.ErrorLabel.setText("Formato de cedula incorrecto");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void ModifyClient() {
        try {
            CM = CP.getClientData();
            String ID_User = CM.getID();
            String ID_Ced = CM.getCed();
            String Name = CM.getName();
            String LastName = CM.getLastName();
            String Age = CM.getAge();
            String Phone = CM.getPhone();
            String Mail = CM.getMail();
            String Direction = CM.getDirection();

            if (ID_Ced.length() == 14) {
                if (!Character.isLetter(ID_Ced.charAt(13))) {
                    CP.ErrorLabel.setVisible(true);
                    CP.ErrorLabel.setText("Formato de cedula incorrecto");
                } else {
                    CallableStatement cs = con.prepareCall("{call ClientUpdate (?,?,?,?,?,?,?,?)}");

                    cs.setString(1, CM.getID());
                    cs.setString(2, CM.getCed());
                    cs.setString(3, CM.getName());
                    cs.setString(4, CM.getLastName());
                    cs.setString(5, CM.getAge());
                    cs.setString(6, CM.getDirection());
                    cs.setString(7, CM.getMail());
                    cs.setString(8, CM.getPhone());
                    
                    ResultSet rs = cs.executeQuery();

                    if (rs.next()) {
                        String valida = rs.getString("CODIGO");

                        if (valida.equals("0")) {
                            STB.LoadClientTable(CP.ClientTable);
                            CP.ErrorLabel.setVisible(true);
                            CP.ErrorLabel.setText("Client Modified Correctly");
                            CP.Clean();
                        } else if (valida.equals("1")) {
                            CP.ErrorLabel.setVisible(true);
                            CP.ErrorLabel.setText("Client Already Exists");
                        }
                    }
                }
            } else {
                CP.ErrorLabel.setVisible(true);
                CP.ErrorLabel.setText("Formato de cedula incorrecto");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void DeleteClient() {
        try {
            CM = CP.getClientData();
            
            CallableStatement cs = con.prepareCall("{call ClientDelete (?)}");

            cs.setString(1, CM.getCed());
            ResultSet rs = cs.executeQuery();

            if (rs.next()) {
                STB.LoadClientTable(CP.ClientTable);
                CP.ErrorLabel.setVisible(true);
                CP.ErrorLabel.setText(rs.getString(1));
                CP.Clean();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void Clean() {
        CP.Clean();
    }

    Thread tiempo = new Thread() {
        public void run() {
            try {
                for (int i = 1; i <= 100; i++) {
                    CP.ErrorLabel.setText("Entered Correctly");
                    tiempo.sleep(10);
                    if (i == 100) {
                        CP.ErrorLabel.setVisible(false);
                    }
                }
            } catch (Exception e) {
            }
        }
    };
}

package Controllers;

import ConnectionSql.ConnectionSQLServer;
import Misc.ShowTables;
import Models.OptometryModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import paneles.Optometry;

public class OptometryController implements ActionListener {

    Optometry o;
    OptometryModel om;
    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();
    ShowTables STB = new ShowTables();

    public OptometryController(Optometry o) {
        super();
        this.o = o;
        om = new OptometryModel();
        STB.LoadOptometryTable(o.OptometryTable);
    }

    public OptometryModel getOm() {
        return om;
    }

    public void setOm(OptometryModel om) {
        this.om = om;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "addpatients":
                addpatients();
                break;
                
            case "Limpiar":
                o.Clean();
                break;
        }
    }

    private void addpatients() {
        om = o.getOptometryData();

        String ID_Cliente = om.getID_Cliente();
        //String Exam_Date = om.getExam_Date();
        String Eyes_Right = om.getEyes_Right();
        String Eyes_Left = om.getEyes_Left();
        String Distance = om.getDistance();
        String Lenses_Type = om.getLenses_Type();
        String Medication = om.getMedication();
        String Observation = om.getObservation();

        try {
            String sql = " EXEC OptometryInsert '" + ID_Cliente + "','" + Eyes_Right + "','" + Eyes_Left + "','" + Distance + "','" + Lenses_Type + "','" + Medication + "','" + Observation + "'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
            if(ID_Cliente.isEmpty()||Eyes_Right.isEmpty()||Eyes_Left.isEmpty()||Distance.isEmpty()||Lenses_Type.isEmpty()||Medication.isEmpty()){
                o.errorLabel.setText("Porfavor rellene todos los campos para continuar");
                for(int i=0;i<1000;i++){
                    o.errorLabel.setText("");
                }
            }else if(rs.next()){
                    o.errorLabel.setText(rs.getString(1));
                    STB.LoadOptometryTable(o.OptometryTable);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}

package Controllers;

import ConnectionSql.ConnectionSQLServer;
import Misc.ShowTables;
import Models.InventoryModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import javax.swing.JOptionPane;
import paneles.inventoryPanel;

public class InventoryController implements ActionListener{

    ConnectionSQLServer csql = new ConnectionSQLServer();
    Connection con = csql.getConnection();
    
    inventoryPanel IP;
    InventoryModel IM;
    ShowTables ST = new ShowTables();
    
    public InventoryController(inventoryPanel IP){
        super();
        this.IP = IP;
        IM = new InventoryModel();
    }

    public InventoryModel getIM() {
        return IM;
    }

    public void setIM(InventoryModel IM) {
        this.IM = IM;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
      switch(e.getActionCommand()){
          case "Guardar":
              IM = IP.getInventoryData();
                if(IM.getId().equals("0")){
                    Save();
                }else{
                    Update();
                }
              break;
              
          case "Eliminar":
              Delete();
              break;
      } 
    }
    
    public void Save(){
        IM = IP.getInventoryData();
        
        try {
            CallableStatement cs = con.prepareCall("{call InventoryInsert (?,?,?,?)}");
            
            cs.setString(1, IM.getCode());
            cs.setString(2, IM.getNombre());
            cs.setString(3, IM.getPrecio());
            cs.setString(4, IM.getCantidad());
            
            ResultSet rs = cs.executeQuery();
            
            if(rs.next()){
                String Mensaje = rs.getString(1);
                JOptionPane.showMessageDialog(null, Mensaje);
                IP.clean();
                ST.InventoryTable(IP.InventoryTable);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void Update(){
                IM = IP.getInventoryData();
        
        try {
            CallableStatement cs = con.prepareCall("{call InventoryUpdate (?,?,?,?,?)}");
            
            cs.setString(1, IM.getId());
            cs.setString(2, IM.getCode());
            cs.setString(3, IM.getNombre());
            cs.setString(4, IM.getPrecio());
            cs.setString(5, IM.getCantidad());
            
            ResultSet rs = cs.executeQuery();
            
            if(rs.next()){
                String Mensaje = rs.getString(1);
                JOptionPane.showMessageDialog(null, Mensaje);
                IP.clean();
                ST.InventoryTable(IP.InventoryTable);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void Delete(){
         IM = IP.getInventoryData();
        
        try {
            CallableStatement cs = con.prepareCall("{call InventoryDelete (?)}");
            
            cs.setString(1, IM.getId());
            
            ResultSet rs = cs.executeQuery();
            
            if(rs.next()){
                String Mensaje = rs.getString(1);
                JOptionPane.showMessageDialog(null, Mensaje);
                IP.clean();
                ST.InventoryTable(IP.InventoryTable);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

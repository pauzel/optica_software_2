package Controllers;

import View.AdminForm;
import View.LoginForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import paneles.CambiaPanel;

public class AdminController implements ActionListener{
    AdminForm AF;
    
    public AdminController(AdminForm AF){
        super();
        this.AF = AF;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "HomeButton":
                Home();
                break;
            case "InventoryButton":
                Inventory();
                break;
            case "SalesRecordButton":
                SalesRecord();
                break;
            case "Optometry":
                Optometry();
                break;
            case "UserRegistrationButton":
                UserRegistration();
                break;
            case "RegisterClientButton":
                RegisterClient();
                break;
            case "LogOutButton":
                LogOut();
                break;
            case "aboutButton":
                About();
                break;
        }
    }
    
    private void Home(){
        CambiaPanel cambiaPanel = new CambiaPanel(AF.principaljPanel, new paneles.homePanel());
    }
    
    private void Inventory(){
        CambiaPanel cambiaPanel = new CambiaPanel(AF.principaljPanel, new paneles.inventoryPanel());
    }
    
    private void SalesRecord(){
        CambiaPanel cambiaPanel = new CambiaPanel(AF.principaljPanel, new paneles.Sales());
    }
    
    private void Optometry(){
        CambiaPanel cambiaPanel = new CambiaPanel(AF.principaljPanel,new paneles.Optometry());
    }
    private void UserRegistration(){
        CambiaPanel cambiaPanel = new CambiaPanel(AF.principaljPanel, new paneles.Usuarios());
    }
    
    private void RegisterClient(){
        CambiaPanel cambiaPanel = new CambiaPanel(AF.principaljPanel, new paneles.clientPanel());
    }
    private void About(){
        CambiaPanel cambiaPanel = new CambiaPanel(AF.principaljPanel, new paneles.Usuarios());
    }
    
    private void LogOut(){
        int confirmacion = JOptionPane.showConfirmDialog(null,  "¿Desea Cerrar Sesion?","" ,JOptionPane.YES_NO_OPTION);
       
       switch (confirmacion){
           case 0:{
               
               LoginForm l = new LoginForm();
               l.setVisible(true);
               AF.dispose();
               break;
           }  
       }
    }   
}

package Controllers;

import ConnectionSql.ConnectionSQLServer;
import Misc.getData;
import Models.ChangePasswordModel;
import Models.UserRegistrationAdminEmployeeModel;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;
import paneles.ChangePassword;

public class ChangePasswordController implements ActionListener {

    private final ChangePassword cp;
    ChangePasswordModel cpm;
    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();

    public ChangePasswordController(ChangePassword cp) {
        super();
        this.cp = cp;
        cpm = new ChangePasswordModel();
    }

    public ChangePasswordModel getCpm() {
        return cpm;
    }

    public void setCpm(ChangePasswordModel cpm) {
        this.cpm = cpm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Change":
                change();
                break;

            case "exit":
                cp.dispose();
                break;
        }
    }

    public void change() {

        cpm = cp.getData();

        String oldp = cpm.getOldPassword();
        String newp = cpm.getNewPassword();

        try {
            String sql = "DECLARE  @n TINYINT, @nm VARCHAR(500);"
                    + "EXEC ChangePassword '" + oldp + "','" + newp + "',@n OUTPUT,@nm OUTPUT;"
                    + "select @n Codigo,@nm Mensaje";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                String Rotulo = rs.getString("Codigo");

                if (Rotulo.equals("0")) {
                    cp.errorLabel.setText(rs.getString("Mensaje"));
                    Enviar();
                } else {
                    cp.errorLabel.setText(rs.getString("Mensaje"));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void Enviar() {
        try {
            cpm = cp.getData();
            getData gd = new getData();
            String newp = cpm.getNewPassword();

            String SQL1 = "SELECT * FROM dbo.ViewUsers WHERE [Password] = HASHBYTES('SHA2_256','" + newp + "')";
            Statement stmt = con.createStatement();
            ResultSet rs1 = stmt.executeQuery(SQL1);

            while (rs1.next()) {
                Properties propiedad = new Properties();
                propiedad.setProperty("mail.smtp.host", "smtp.gmail.com");
                propiedad.setProperty("mail.smtp.starttls.enable", "true");
                propiedad.setProperty("mail.smtp.port", "587");
                propiedad.setProperty("mail.smtp.auth", "true");

                Session sesion = Session.getDefaultInstance(propiedad);
                String correoEnvia = "opticascalero@gmail.com";
                String contraseña = "123Putito123";
                String destinatario = rs1.getString("Mail");
                String Asunto = "Change of password";

                MimeMessage mail = new MimeMessage(sesion);
                try {
                    mail.setFrom(new InternetAddress(correoEnvia));
                    mail.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
                    mail.setSubject(Asunto);
                    mail.setText("Dear "+ rs1.getString("Name") + " " + rs1.getString("LastName") + ", you are hereby informed that the password has been changed successfully.\n"
                            + "\n"
                            + "If you have not made the change please write to us at: opticascalero@gmail.com\n"
                            + "\n"
                            + "If you made the change, ignore this email.\n"
                            + "\n"
                            + "Excellent day, Optica Calero.\n\n\n"
                            + "------------------------------------------------\n"
                            + "Additional data:\n"
                            + "\nHost Name: "+ gd.Obtener_Nombre()
                            + "\nPublic IP: "+ gd.ObtenerIP_Publica()
                            + "\nLocal IP: "+gd.ObtenerIP_Local()
                            + "\nMore Information: https://whatismyipaddress.com/ip/"+gd.ObtenerIP_Publica()
                            + "\n------------------------------------------------");

                    Transport transportar = sesion.getTransport("smtp");
                    transportar.connect(correoEnvia, contraseña);
                    transportar.sendMessage(mail, mail.getRecipients(Message.RecipientType.TO));
                    transportar.close();

                    //JOptionPane.showMessageDialog(null, "Please check email and whatsapp ", "Notification", JOptionPane.INFORMATION_MESSAGE);

                } catch (AddressException ex) {
                    Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
                    System.out.println(ex.getMessage());
                } catch (MessagingException ex) {
                    Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
                     System.out.println(ex.getMessage());
                }
            }

            rs1.close();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
             System.out.println(e.getMessage());
        }
    }

}

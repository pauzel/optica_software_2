package Controllers;

import ConnectionSql.ConnectionSQLServer;
import Misc.ShowTables;
import Models.SalesModel;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import paneles.Sales;

public class SalesController implements ActionListener {

    Sales s;
    SalesModel sm;
    ShowTables STB = new ShowTables();
    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();

    public SalesController(Sales s) {
        super();
        this.s = s;
        sm = new SalesModel();
        STB.LoadClientTable(s.salesTable);
    }

    public SalesModel getSm() {
        return sm;
    }

    public void setSm(SalesModel sm) {
        this.sm = sm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "SearchID":
                searchid();
                break;
            case "SearchCode":
                searchcode();
                break;
            case "GenerateSales":
                generatesales();
                break;
        }
    }

    public void searchid() {
        sm = s.getSalesData();

        String ID_Client = sm.getID_Client();

        try {
            String sql = " EXEC ClientSearchIDOptometry '" + ID_Client + "'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                s.ClienteTextField.setText(rs.getString(1));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void searchcode() {
        sm = s.getSalesData();

        String Code = sm.getCode_Prod();

        try {
            String sql = " EXEC InventorySearchSales '" + Code + "'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                s.brandTextField.setText(rs.getString("Brand"));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void generatesales() {
        sm = s.getSalesData();

        String ID_Client = sm.getID_Client();
        String Exam_Date = sm.getDate_Exam();
        String Medication = sm.getMedication();
        String Observation = sm.getObservation();
        String Code = sm.getCode_Prod();
        String Brand = sm.getBrand();
        String Color = sm.getColor();

        try {
            String sql = "DECLARE @n TINYINT,@nm VARCHAR(500);"
                    + " EXEC SalesInsert '" + ID_Client + "','" + Code + "',@n OUTPUT,@nm OUTPUT;"
                    + "Select @n Codigo,@nm Mensaje";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                String bandera = rs.getString("Codigo");

                if (bandera.equals("0")) {
                    //s.errorLabel.setText(rs.getString("Mensaje"));
                    STB.LoadSalesTable(s.salesTable);
                } else if (bandera.equals("1")) {
                    //s.errorLabel.setText(rs.getString("Mensaje"));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private Image getImage(byte[] bytes, boolean isThumbnail) throws IOException {

        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        Iterator readers = ImageIO.getImageReadersByFormatName("png");
        ImageReader reader = (ImageReader) readers.next();
        Object source = bis; // File or InputStream
        ImageInputStream iis = ImageIO.createImageInputStream(source);
        reader.setInput(iis, true);
        ImageReadParam param = reader.getDefaultReadParam();
        if (isThumbnail) {

            param.setSourceSubsampling(4, 4, 0, 0);

        }
        return reader.read(0, param);

    }

}

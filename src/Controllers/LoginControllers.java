package Controllers;

import ConnectionSql.ConnectionSQLServer;
import Models.LoginModel;
import View.AdminForm;
import View.EmployeeForm;
import View.LoginForm;
import java.awt.event.*;
import java.sql.*;
import javax.swing.JOptionPane;

public class LoginControllers implements ActionListener {

    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();
    
    LoginForm LF;
    LoginModel LM;

    public LoginControllers(LoginForm LF) {
        super();
        this.LF = LF;
        LM = new LoginModel();
        LF.error.setVisible(false);
    }

    public LoginModel getLM() {
        return LM;
    }

    public void setLM(LoginModel LM) {
        this.LM = LM;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Login":
                login();
                break;
        }
    }

    public void login() {
        LM = LF.GetData();

        try {
            String User = LM.getUser();
            String Password = LM.getPassword();

            String sql = " DECLARE @n TINYINT,@nm VARCHAR(500)"
                    + " EXEC LoginProgram '" + User + "','" + Password + "',@n OUTPUT,@nm OUTPUT "
                    + " SELECT @n Codigo,@nm Mensaje";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
            if(User.isEmpty()){
                LF.error.setVisible(true);
                LF.error.setText("User is empty");
                LF.Clean();
            }else if(Password.isEmpty()){
                LF.error.setVisible(true);
                LF.error.setText("Password is empty");
                LF.Clean();
            }else if (rs.next()) {
                String Rotulo = rs.getString("Codigo");
                if (Rotulo.equals("1")) {
                    LF.error.setVisible(true);
                    LF.error.setText(rs.getString("Mensaje"));
                    LF.Clean();
                } else if (Rotulo.equals("0")) {
                    String sql1 = "EXEC LoginFull '" + User + "'";
                    Statement st1 = con.createStatement();
                    ResultSet rs1 = st1.executeQuery(sql1);
                    if (rs1.next()) {
                        String Role = rs1.getString("Role");
                        if (Role.equals("Admin")) {
                            AdminForm AF = new AdminForm();
                            AF.setVisible(true);
                            LF.dispose();
                        } else if (Role.equals("Employee")) {
                            EmployeeForm EF = new EmployeeForm();
                            EF.setVisible(true);
                            LF.dispose();
                        }
                    }
                }
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

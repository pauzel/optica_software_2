package Models;

public class ChangePasswordModel {
    String OldPassword;
    String NewPassword;

    public ChangePasswordModel(String OldPassword, String NewPassword) {
        this.OldPassword = OldPassword;
        this.NewPassword = NewPassword;
    }
    
    public ChangePasswordModel() {
        this.OldPassword = "";
        this.NewPassword = "";
    } 

    public String getOldPassword() {
        return OldPassword;
    }

    public void setOldPassword(String OldPassword) {
        this.OldPassword = OldPassword;
    }

    public String getNewPassword() {
        return NewPassword;
    }

    public void setNewPassword(String NewPassword) {
        this.NewPassword = NewPassword;
    }
    
    
}

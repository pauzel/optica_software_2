/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paneles;

import ConnectionSql.ConnectionSQLServer;
import Controllers.UserRegistrationAdminEmployeeController;
import Misc.ShowTables;
import Models.UserRegistrationAdminEmployeeModel;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.Timer;

/**
 *
 * @author AlexPc
 */
public class Usuarios extends javax.swing.JPanel {

    ShowTables st = new ShowTables();
    private UserRegistrationAdminEmployeeController URAEC;
    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();

    public Usuarios() {
        initComponents();
        this.PasswordPasswordField.setEchoChar((char) 0);
        setController();
        performance();
        this.showItems();
    }

    private void performance() {
        st.LoadUserTable(UserTable);
        idTextField.setBackground(new java.awt.Color(0, 0, 0, 1));
        cedFormatTextField.setBackground(new java.awt.Color(0, 0, 0, 1));
        UserTextField.setBackground(new java.awt.Color(0, 0, 0, 1));
        PasswordPasswordField.setBackground(new java.awt.Color(0, 0, 0, 1));
        nameTextField.setBackground(new java.awt.Color(0, 0, 0, 1));
        lastnameTextField.setBackground(new java.awt.Color(0, 0, 0, 1));
        PhoneFormattedTextField.setBackground(new java.awt.Color(0, 0, 0, 1));
        emailTextField.setBackground(new java.awt.Color(0, 0, 0, 1));
        DepComboBox.setBackground(new java.awt.Color(0, 0, 0, 1));
        MunComboBox.setBackground(new java.awt.Color(0, 0, 0, 1));
        RoleComboBox.setBackground(new java.awt.Color(0, 0, 0, 1));
        DirectionTextField.setBackground(new java.awt.Color(0, 0, 0, 1));
        searchuserTextField.setBackground(new java.awt.Color(0, 0, 0, 1));
        this.CleanButton.setCursor(new Cursor(HAND_CURSOR));
        this.SaveButton.setCursor(new Cursor(HAND_CURSOR));
        this.ModifyButton.setCursor(new Cursor(HAND_CURSOR));
        this.DeleteButton.setCursor(new Cursor(HAND_CURSOR));
        this.ChangePasswordLabel.setCursor(new Cursor(HAND_CURSOR));
    }

    private void setController() {
        URAEC = new UserRegistrationAdminEmployeeController(this);
        SaveButton.addActionListener(URAEC);
        ModifyButton.addActionListener(URAEC);
        DeleteButton.addActionListener(URAEC);
        CleanButton.addActionListener(URAEC);
    }

    public void setUserData(UserRegistrationAdminEmployeeModel URAEM) {
        idTextField.setText(URAEM.getID_User());
        cedFormatTextField.setText(URAEM.getID_Ced());
        UserTextField.setText(URAEM.getUser());
        PasswordPasswordField.setText(URAEM.getPassword());
        nameTextField.setText(URAEM.getName());
        lastnameTextField.setText(URAEM.getLastName());
        PhoneFormattedTextField.setText(URAEM.getPhone());
        emailTextField.setText(URAEM.getMail());
        DepComboBox.setSelectedItem(URAEM.getDepartment());
        MunComboBox.setSelectedItem(URAEM.getMunicipality());
        RoleComboBox.setSelectedItem(URAEM.getRole());
        DirectionTextField.setText(URAEM.getDirection());
    }

    public UserRegistrationAdminEmployeeModel getUserData() {
        UserRegistrationAdminEmployeeModel URAEM = new UserRegistrationAdminEmployeeModel();
        URAEM.setID_User(idTextField.getText());
        URAEM.setID_Ced(cedFormatTextField.getText());
        URAEM.setUser(UserTextField.getText());
        URAEM.setPassword(String.valueOf(PasswordPasswordField.getPassword()));
        URAEM.setName(nameTextField.getText());
        URAEM.setLastName(lastnameTextField.getText());
        URAEM.setPhone(PhoneFormattedTextField.getText());
        URAEM.setMail(emailTextField.getText());
        URAEM.setDepartment(DepComboBox.getSelectedItem().toString());
        URAEM.setMunicipality(MunComboBox.getSelectedItem().toString());
        URAEM.setRole(RoleComboBox.getSelectedItem().toString());
        URAEM.setDirection(DirectionTextField.getText());
        return URAEM;

    }

    public void Clean() {
        idTextField.setText("");
        cedFormatTextField.setText("");
        UserTextField.setText("");
        PasswordPasswordField.setText("");
        nameTextField.setText("");
        lastnameTextField.setText("");
        PhoneFormattedTextField.setText("");
        emailTextField.setText("");
        DepComboBox.setSelectedItem("");
        MunComboBox.setSelectedItem("");
        RoleComboBox.setSelectedItem("");
        DirectionTextField.setText("");
        errorLabel.setVisible(false);
    }

    public void showItems(String D) {
        int i;
        DepComboBox.removeAllItems();
        ArrayList<String> lista = new ArrayList<String>();
        lista = fullD(D);
        for (i = 0; i < lista.size(); i++) {
            DepComboBox.addItem(lista.get(i));
        }

    }

    public void showItems() {
        int i;
        DepComboBox.removeAllItems();
        ArrayList<String> lista = new ArrayList<String>();
        lista = fullD();
        for (i = 0; i < lista.size(); i++) {
            DepComboBox.addItem(lista.get(i));
        }

    }

    public ArrayList<String> fullD(String D) {
        ArrayList<String> Lista = new ArrayList<String>();
        try {
            String q = "SELECT * FROM dbo.departments Where Name_Departments = ?";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(q);

            ps.setString(1, D);
            rs = ps.executeQuery();
            while (rs.next()) {
                Lista.add(rs.getString("Name_Departments"));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return Lista;
    }

    public ArrayList<String> fullD() {
        ArrayList<String> Lista = new ArrayList<String>();
        try {
            String q = "SELECT * FROM dbo.departments";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(q);
            rs = ps.executeQuery();
            while (rs.next()) {
                Lista.add(rs.getString("Name_Departments"));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return Lista;
    }

    public ArrayList<String> fullM(String name) {
        ArrayList<String> Lista = new ArrayList<String>();
        try {
            String q = "SELECT m.Name_Municipality FROM dbo.departments d\n"
                    + "INNER JOIN dbo.Municipality M ON M.ID = d.ID\n"
                    + "WHERE d.Name_Departments = ?";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(q);
            ps.setString(1, name);
            rs = ps.executeQuery();
            while (rs.next()) {
                Lista.add(rs.getString("Name_Municipality"));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return Lista;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PhoneFormattedTextField = new javax.swing.JTextField();
        emailTextField = new javax.swing.JTextField();
        MunComboBox = new javax.swing.JComboBox<>();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        RoleComboBox = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        idTextField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        cedFormatTextField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        UserTextField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        ChangePasswordLabel = new javax.swing.JButton();
        DepComboBox = new javax.swing.JComboBox<>();
        nameTextField = new javax.swing.JTextField();
        lastnameTextField = new javax.swing.JTextField();
        DirectionTextField = new javax.swing.JTextField();
        CleanButton = new javax.swing.JButton();
        DeleteButton = new javax.swing.JButton();
        ModifyButton = new javax.swing.JButton();
        SaveButton = new javax.swing.JButton();
        PasswordPasswordField = new javax.swing.JPasswordField();
        errorLabel = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        searchuserTextField = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        UserTable = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(36, 148, 216));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        add(PhoneFormattedTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 50, 120, -1));
        add(emailTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 90, 160, -1));

        MunComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        add(MunComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 170, 120, -1));

        jLabel12.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel12.setText("Teléfono");
        add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 50, -1, -1));

        jLabel13.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel13.setText("Correo");
        add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 90, -1, -1));

        jLabel14.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel14.setText("Departamento");
        add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 130, -1, -1));

        jLabel15.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel15.setText("Municipio");
        add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 170, -1, -1));

        RoleComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Admin", "Employee" }));
        add(RoleComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 130, 160, -1));

        jLabel3.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel3.setText("ID usuario");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 50, -1, -1));

        idTextField.setEditable(false);
        add(idTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 50, 90, -1));

        jLabel4.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel4.setText("ID cedula");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 90, -1, -1));
        add(cedFormatTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 90, 150, -1));

        jLabel6.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel6.setText("Usuario");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 130, -1, -1));
        add(UserTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 130, 150, -1));

        jLabel5.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel5.setText("Contraseña");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 170, -1, -1));

        jLabel7.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel7.setText("Dirección");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 170, -1, -1));

        jLabel8.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel8.setText("Rol");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 130, -1, -1));

        jLabel9.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel9.setText("Apellidos");
        add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 90, -1, -1));

        jLabel11.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel11.setText("Nombre");
        add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 50, -1, -1));

        ChangePasswordLabel.setFont(new java.awt.Font("Lato", 2, 10)); // NOI18N
        ChangePasswordLabel.setForeground(new java.awt.Color(0, 153, 204));
        ChangePasswordLabel.setText("Cambiar contraseña");
        ChangePasswordLabel.setBorderPainted(false);
        ChangePasswordLabel.setContentAreaFilled(false);
        ChangePasswordLabel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChangePasswordLabelActionPerformed(evt);
            }
        });
        add(ChangePasswordLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 200, -1, 20));

        DepComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        DepComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DepComboBoxActionPerformed(evt);
            }
        });
        add(DepComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 130, 120, -1));
        add(nameTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 50, 160, -1));
        add(lastnameTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 90, 160, -1));
        add(DirectionTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 170, 160, -1));

        CleanButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/limpiar.png"))); // NOI18N
        CleanButton.setActionCommand("CleanButton");
        CleanButton.setBorderPainted(false);
        CleanButton.setContentAreaFilled(false);
        add(CleanButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 200, -1, -1));

        DeleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/eliminar.png"))); // NOI18N
        DeleteButton.setActionCommand("DeleteButton");
        DeleteButton.setBorderPainted(false);
        DeleteButton.setContentAreaFilled(false);
        add(DeleteButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 200, 130, 40));

        ModifyButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/modificar.png"))); // NOI18N
        ModifyButton.setActionCommand("ModifyButton");
        ModifyButton.setBorderPainted(false);
        ModifyButton.setContentAreaFilled(false);
        add(ModifyButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 200, 120, 40));

        SaveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/guardar.png"))); // NOI18N
        SaveButton.setActionCommand("InsertButton");
        SaveButton.setBorderPainted(false);
        SaveButton.setContentAreaFilled(false);
        add(SaveButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 200, 120, 40));
        add(PasswordPasswordField, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 170, 150, -1));

        errorLabel.setForeground(new java.awt.Color(255, 51, 51));
        errorLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        add(errorLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 0, 310, 30));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/barra 3.png"))); // NOI18N
        add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 10, 870, 250));
        add(searchuserTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 290, 230, 30));

        UserTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "ID usuario", "ID cedula", "Usuario", "Contraseña", "Nombre", "Apellidos", "Telefono", "Correo", "Departamento", "Municipio", "Rol", "Dirección"
            }
        ));
        UserTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                UserTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(UserTable);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 340, 820, 230));

        jLabel2.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel2.setText("Buscar");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 290, -1, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/barra 4.png"))); // NOI18N
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 260, 870, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void ChangePasswordLabelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChangePasswordLabelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ChangePasswordLabelActionPerformed

    private void UserTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_UserTableMouseClicked
        int Selection = UserTable.rowAtPoint(evt.getPoint());
        idTextField.setText(String.valueOf(UserTable.getValueAt(Selection, 0)));
        cedFormatTextField.setText(String.valueOf(UserTable.getValueAt(Selection, 1)));
        UserTextField.setText(String.valueOf(UserTable.getValueAt(Selection, 2)));
        nameTextField.setText(String.valueOf(UserTable.getValueAt(Selection, 4)));
        lastnameTextField.setText(String.valueOf(UserTable.getValueAt(Selection, 5)));
        PhoneFormattedTextField.setText(String.valueOf(UserTable.getValueAt(Selection, 6)));
        emailTextField.setText(String.valueOf(UserTable.getValueAt(Selection, 7)));
        DepComboBox.setSelectedItem(UserTable.getValueAt(Selection, 8));
        MunComboBox.setSelectedItem(UserTable.getValueAt(Selection, 9));
        RoleComboBox.setSelectedItem(UserTable.getValueAt(Selection, 10));
        DirectionTextField.setText(String.valueOf(UserTable.getValueAt(Selection, 11)));
    }//GEN-LAST:event_UserTableMouseClicked

    private void DepComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DepComboBoxActionPerformed
        MunComboBox.removeAllItems();
        DepComboBox.addItem("");
        int i;
        MunComboBox.removeAllItems();
        ArrayList<String> lista = new ArrayList<String>();
        String nombre = DepComboBox.getSelectedItem().toString();
        lista = fullM(nombre);
        for (i = 0; i < lista.size(); i++) {
            MunComboBox.addItem(lista.get(i));
        }
    }//GEN-LAST:event_DepComboBoxActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ChangePasswordLabel;
    private javax.swing.JButton CleanButton;
    private javax.swing.JButton DeleteButton;
    private javax.swing.JComboBox<String> DepComboBox;
    private javax.swing.JTextField DirectionTextField;
    private javax.swing.JButton ModifyButton;
    private javax.swing.JComboBox<String> MunComboBox;
    private javax.swing.JPasswordField PasswordPasswordField;
    private javax.swing.JTextField PhoneFormattedTextField;
    private javax.swing.JComboBox<String> RoleComboBox;
    private javax.swing.JButton SaveButton;
    public javax.swing.JTable UserTable;
    private javax.swing.JTextField UserTextField;
    private javax.swing.JTextField cedFormatTextField;
    private javax.swing.JTextField emailTextField;
    public javax.swing.JLabel errorLabel;
    private javax.swing.JTextField idTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField lastnameTextField;
    private javax.swing.JTextField nameTextField;
    private javax.swing.JTextField searchuserTextField;
    // End of variables declaration//GEN-END:variables
}

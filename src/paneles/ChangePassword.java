package paneles;

import Controllers.ChangePasswordController;
import Models.ChangePasswordModel;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class ChangePassword extends javax.swing.JFrame {

    public ChangePassword() {
        initComponents();
        setController();
        this.setResizable(false); //EVITAMOS QUE EL JFRAME SE MAXIMIZE
        this.setLocationRelativeTo(this);
        this.changeButton.setCursor(new Cursor(HAND_CURSOR));
        this.exitButton.setCursor(new Cursor(HAND_CURSOR));
    }
    
    public void setController(){
        ChangePasswordController cpc = new ChangePasswordController(this);
        
        changeButton.addActionListener(cpc);
        exitButton.addActionListener(cpc);
    }
    
    public ChangePasswordModel getData(){
        ChangePasswordModel cpm = new ChangePasswordModel();
        
        cpm.setOldPassword(oldpasswordTextField.getText());
        cpm.setNewPassword(newpasswordTextField.getText());
        
        return cpm;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        newpasswordTextField = new javax.swing.JTextField();
        oldpasswordTextField = new javax.swing.JTextField();
        changeButton = new javax.swing.JButton();
        exitButton = new javax.swing.JButton();
        errorLabel = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel4.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Old Password");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 50, -1, 30));

        jLabel5.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("New Password");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 90, -1, 30));

        newpasswordTextField.setBackground(new java.awt.Color(39, 7, 82));
        newpasswordTextField.setFont(new java.awt.Font("Calibri", 1, 11)); // NOI18N
        newpasswordTextField.setForeground(new java.awt.Color(255, 255, 255));
        newpasswordTextField.setBorder(null);
        getContentPane().add(newpasswordTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 90, 190, 20));

        oldpasswordTextField.setBackground(new java.awt.Color(39, 7, 82));
        oldpasswordTextField.setFont(new java.awt.Font("Calibri", 1, 11)); // NOI18N
        oldpasswordTextField.setForeground(new java.awt.Color(255, 255, 255));
        oldpasswordTextField.setBorder(null);
        getContentPane().add(oldpasswordTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 50, 190, 20));

        changeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/changepasswordButton.png"))); // NOI18N
        changeButton.setActionCommand("Change");
        changeButton.setBorderPainted(false);
        changeButton.setContentAreaFilled(false);
        changeButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                changeButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                changeButtonMouseExited(evt);
            }
        });
        getContentPane().add(changeButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 130, 150, 30));

        exitButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/exit.png"))); // NOI18N
        exitButton.setActionCommand("exit");
        exitButton.setBorderPainted(false);
        exitButton.setContentAreaFilled(false);
        getContentPane().add(exitButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 0, 30, 30));

        errorLabel.setBackground(new java.awt.Color(255, 0, 51));
        errorLabel.setForeground(new java.awt.Color(204, 0, 0));
        errorLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        getContentPane().add(errorLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 20, 240, 20));
        getContentPane().add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 110, 190, 10));
        getContentPane().add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 70, 190, 10));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/background_changepassword.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 200));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void changeButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_changeButtonMouseEntered
        changeButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/changepasswordButtonefecto.png")));
    }//GEN-LAST:event_changeButtonMouseEntered

    private void changeButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_changeButtonMouseExited
       changeButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/changepasswordButton.png")));
    }//GEN-LAST:event_changeButtonMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ChangePassword.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ChangePassword.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ChangePassword.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ChangePassword.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ChangePassword().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton changeButton;
    public javax.swing.JLabel errorLabel;
    private javax.swing.JButton exitButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField newpasswordTextField;
    private javax.swing.JTextField oldpasswordTextField;
    // End of variables declaration//GEN-END:variables
}

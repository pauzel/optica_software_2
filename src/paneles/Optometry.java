package paneles;

import ConnectionSql.ConnectionSQLServer;
import Controllers.OptometryController;
import Misc.DesignTable;
import Misc.ShowTables;
import Models.OptometryModel;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.Icon;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class Optometry extends javax.swing.JPanel {

    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();
    ShowTables STB = new ShowTables();

    public Optometry() {
        initComponents();
        performance();
    }

    public void performance() {
        setController();
        STB.LoadOptometryTable(OptometryTable);
        addpatientsButton.setCursor(new Cursor(HAND_CURSOR));
        LimpiarButton.setCursor(new Cursor(HAND_CURSOR));
    }

    public void setController() {
        OptometryController oc = new OptometryController(this);
        addpatientsButton.addActionListener(oc);
        LimpiarButton.addActionListener(oc);
    }

    public OptometryModel getOptometryData() {
        OptometryModel om = new OptometryModel();
        String EyeR, EyeL, Medication;
        String ar = null, br = null, phc = null;
        EyeR = erefTextField.getText() + ", " + erciTextField.getText() + ", " + erejeTextField.getText() + ", " + eraddTextField.getText();
        EyeL = elefTextField.getText() + ", " + elciTextField.getText() + ", " + elejeTextField.getText() + ", " + eladdTextField.getText();

        if (arCheckBox.isSelected()) {
            ar = "AR";
        } else {
            ar = "";
        }
        if (brCheckBox.isSelected()) {
            br = "BR";
        } else {
            br = "";
        }
        if (phcCheckBox.isSelected()) {
            phc = "PHC";
        } else {
            phc = "";
        }

        Medication = ar + "  " + br + "  " + phc;

        om.setID_Cliente(ID_ClientTextField.getText());
        om.setEyes_Right(EyeR);
        om.setEyes_Left(EyeL);
        om.setDistance(distanceComboBox.getSelectedItem().toString());
        om.setLenses_Type(lensestypeComboBox.getSelectedItem().toString());
        om.setMedication(Medication);
        om.setObservation(observacionTextArea.getText());
        return om;
    }

    public void Clean() {
        this.ID_ClientTextField.setText("");
        erefTextField.setText("");
        erciTextField.setText("");
        erejeTextField.setText("");
        eraddTextField.setText("");
        elefTextField.setText("");
        elciTextField.setText("");
        elejeTextField.setText("");
        eladdTextField.setText("");
        this.observacionTextArea.setText("");
        distanceComboBox.setSelectedItem(0);
        lensestypeComboBox.setSelectedItem(0);
        arCheckBox.setSelected(false);
        brCheckBox.setSelected(false);
        phcCheckBox.setSelected(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        OptometryTable = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        LimpiarButton = new javax.swing.JButton();
        addpatientsButton = new javax.swing.JButton();
        observacionTextArea = new javax.swing.JTextField();
        phcCheckBox = new javax.swing.JCheckBox();
        brCheckBox = new javax.swing.JCheckBox();
        arCheckBox = new javax.swing.JCheckBox();
        lensestypeComboBox = new javax.swing.JComboBox<>();
        distanceComboBox = new javax.swing.JComboBox<>();
        elefTextField = new javax.swing.JTextField();
        elciTextField = new javax.swing.JTextField();
        elejeTextField = new javax.swing.JTextField();
        eladdTextField = new javax.swing.JTextField();
        eraddTextField = new javax.swing.JTextField();
        erejeTextField = new javax.swing.JTextField();
        erciTextField = new javax.swing.JTextField();
        erefTextField = new javax.swing.JTextField();
        ID_ClientTextField = new javax.swing.JTextField();
        jTextField1 = new javax.swing.JTextField();
        errorLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(36, 148, 216));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        OptometryTable.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        OptometryTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "ID Exam", "ID cliente", "Fecha Examen", "Ojo Derecho", "Ojo Izquierdo", "Tipo de lentes", "Observación", "Distancia", "Tratamiento"
            }
        ));
        jScrollPane2.setViewportView(OptometryTable);

        add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 330, 800, 240));

        jLabel3.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel3.setText("Buscar");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 287, -1, 30));

        jLabel4.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel4.setText("Ojo derecho");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 180, 90, 20));

        jLabel5.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel5.setText("Tipos de lentes");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 60, 100, 20));

        jLabel6.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel6.setText("ADD");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 100, 50, 20));

        jLabel7.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel7.setText("Ojo izquierdo");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 130, 90, 20));

        jLabel8.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel8.setText("EF");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 100, 30, 20));

        jLabel9.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel9.setText("CI");
        add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 100, 30, 20));

        jLabel10.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel10.setText("EJE");
        add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 100, 30, 20));

        jLabel11.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel11.setText("ID Cliente");
        add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 60, 70, 20));

        jLabel12.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel12.setText("Observación");
        add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 130, 90, 20));

        jLabel13.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel13.setText("Distancia");
        add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 60, 70, 20));

        jLabel14.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel14.setText("Tratamiento");
        add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 130, 90, 20));

        LimpiarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/limpiar.png"))); // NOI18N
        LimpiarButton.setActionCommand("Limpiar");
        LimpiarButton.setBorderPainted(false);
        LimpiarButton.setContentAreaFilled(false);
        add(LimpiarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 200, 120, 40));

        addpatientsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/guardar.png"))); // NOI18N
        addpatientsButton.setActionCommand("addpatients");
        addpatientsButton.setBorderPainted(false);
        addpatientsButton.setContentAreaFilled(false);
        add(addpatientsButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 200, 120, 40));
        add(observacionTextArea, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 100, 140, 120));

        phcCheckBox.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        phcCheckBox.setText("PHC");
        add(phcCheckBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 160, -1, -1));

        brCheckBox.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        brCheckBox.setText("BR");
        add(brCheckBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 130, -1, -1));

        arCheckBox.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        arCheckBox.setText("AR");
        add(arCheckBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 100, -1, -1));

        lensestypeComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Monofocal", "Bifocal", "Progresivo" }));
        add(lensestypeComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 60, 110, 20));

        distanceComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Solo Largo", "Solo Cerca" }));
        add(distanceComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 60, 110, -1));
        add(elefTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 180, 30, -1));
        add(elciTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 180, 30, -1));
        add(elejeTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 180, 30, -1));
        add(eladdTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 180, 30, -1));
        add(eraddTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 130, 30, -1));
        add(erejeTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 130, 30, -1));
        add(erciTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 130, 30, -1));
        add(erefTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 130, 30, -1));
        add(ID_ClientTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 60, 90, -1));
        add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 290, 230, 30));

        errorLabel.setBackground(new java.awt.Color(204, 0, 0));
        errorLabel.setForeground(new java.awt.Color(204, 0, 0));
        add(errorLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 30, 200, 20));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/barra 3.png"))); // NOI18N
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 20, 870, 230));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/barra 4.png"))); // NOI18N
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 260, -1, 340));
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField ID_ClientTextField;
    private javax.swing.JButton LimpiarButton;
    public javax.swing.JTable OptometryTable;
    private javax.swing.JButton addpatientsButton;
    private javax.swing.JCheckBox arCheckBox;
    private javax.swing.JCheckBox brCheckBox;
    private javax.swing.JComboBox<String> distanceComboBox;
    private javax.swing.JTextField eladdTextField;
    private javax.swing.JTextField elciTextField;
    private javax.swing.JTextField elefTextField;
    private javax.swing.JTextField elejeTextField;
    private javax.swing.JTextField eraddTextField;
    private javax.swing.JTextField erciTextField;
    private javax.swing.JTextField erefTextField;
    private javax.swing.JTextField erejeTextField;
    public javax.swing.JLabel errorLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JComboBox<String> lensestypeComboBox;
    private javax.swing.JTextField observacionTextArea;
    private javax.swing.JCheckBox phcCheckBox;
    // End of variables declaration//GEN-END:variables
}

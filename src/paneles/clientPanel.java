package paneles;

import ConnectionSql.ConnectionSQLServer;
import Controllers.ClientController;
import Misc.DesignTable;
import Misc.ShowTables;
import Models.ClientModel;
import java.awt.Color;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;
import java.util.Vector;
import javax.swing.Icon;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class clientPanel extends javax.swing.JPanel {

    ShowTables STB = new ShowTables();
    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();
    
    private ClientController CC;
    
    public clientPanel() {
        initComponents();
        performance();
    }
    
    private void performance(){
        STB.LoadClientTable(ClientTable);
        setController();
        idTextField.setBackground(new java.awt.Color(0,0,0,1));
        cedFormattedTextField.setBackground(new java.awt.Color(0,0,0,1));
        nameTextField.setBackground(new java.awt.Color(0,0,0,1));
        lastnameTextField.setBackground(new java.awt.Color(0,0,0,1));
        ageTextField.setBackground(new java.awt.Color(0,0,0,1));
        directionTextField.setBackground(new java.awt.Color(0,0,0,1));
        mailTextField.setBackground(new java.awt.Color(0,0,0,1));
        phoneFormattedTextField.setBackground(new java.awt.Color(0,0,0,1));
        searchclientTextField.setBackground(new java.awt.Color(0,0,0,1));
       // jSeparator1.setBackground(new java.awt.Color(0,0,0,10));
        this.addclientButton.setCursor(new Cursor(HAND_CURSOR));
        this.modifyClientButton.setCursor(new Cursor(HAND_CURSOR));
        this.deleteClientButton.setCursor(new Cursor(HAND_CURSOR));
        this.CleanButton.setCursor(new Cursor(HAND_CURSOR));
        //this.searchclientButton.setCursor(new Cursor(HAND_CURSOR));
    }
    
    private void setController(){
        CC = new ClientController(this);
        addclientButton.addActionListener(CC);
        modifyClientButton.addActionListener(CC);
        deleteClientButton.addActionListener(CC);
        CleanButton.addActionListener(CC);
    }
    
    public void clientData(ClientModel CM){
        idTextField.setText(CM.getID());
        cedFormattedTextField.setText(CM.getCed());
        nameTextField.setText(CM.getName());
        lastnameTextField.setText(CM.getLastName());
        ageTextField.setText(CM.getAge());
        directionTextField.setText(CM.getDirection());
        mailTextField.setText(CM.getMail());
        phoneFormattedTextField.setText(CM.getPhone());
    }
    
    public ClientModel getClientData(){
        ClientModel CM = new ClientModel();
        CM.setID(idTextField.getText());
        CM.setCed(cedFormattedTextField.getText());
        CM.setName(nameTextField.getText());
        CM.setLastName(lastnameTextField.getText());
        CM.setAge(ageTextField.getText());
        CM.setDirection(directionTextField.getText());
        CM.setMail(mailTextField.getText());
        CM.setPhone(phoneFormattedTextField.getText());
        return CM;
    }
    
    public void Clean(){
        idTextField.setText("");
        cedFormattedTextField.setText("");
        nameTextField.setText("");
        lastnameTextField.setText("");
        ageTextField.setText("");
        directionTextField.setText("");
        mailTextField.setText("");
        phoneFormattedTextField.setText("");
    }
 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        ClientTable = new javax.swing.JTable();
        searchclientTextField = new javax.swing.JTextField();
        lastnameTextField = new javax.swing.JTextField();
        idTextField = new javax.swing.JTextField();
        cedFormattedTextField = new javax.swing.JTextField();
        nameTextField = new javax.swing.JTextField();
        mailTextField = new javax.swing.JTextField();
        directionTextField = new javax.swing.JTextField();
        ageTextField = new javax.swing.JTextField();
        phoneFormattedTextField = new javax.swing.JTextField();
        addclientButton = new javax.swing.JButton();
        deleteClientButton = new javax.swing.JButton();
        CleanButton = new javax.swing.JButton();
        modifyClientButton = new javax.swing.JButton();
        ErrorLabel = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(36, 148, 216));
        setForeground(new java.awt.Color(36, 148, 216));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        ClientTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Id", "Cedula", "Nombre", "Apellidos", "Edad", "Dirección", "Correo", "Teléfono", "Fecha"
            }
        ));
        ClientTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        ClientTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ClientTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(ClientTable);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 340, 820, 230));
        add(searchclientTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 290, 230, 30));
        add(lastnameTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 170, 150, -1));
        add(idTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 50, 90, -1));
        add(cedFormattedTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 90, 150, -1));
        add(nameTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 130, 150, -1));
        add(mailTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 130, 150, -1));
        add(directionTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 90, 150, -1));
        add(ageTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 50, 90, -1));
        add(phoneFormattedTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 170, 150, -1));

        addclientButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/guardar.png"))); // NOI18N
        addclientButton.setActionCommand("AddClient");
        addclientButton.setBorderPainted(false);
        addclientButton.setContentAreaFilled(false);
        add(addclientButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 200, 130, 40));

        deleteClientButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/eliminar.png"))); // NOI18N
        deleteClientButton.setActionCommand("DeleteClient");
        deleteClientButton.setBorderPainted(false);
        deleteClientButton.setContentAreaFilled(false);
        add(deleteClientButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 200, 130, 40));

        CleanButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/limpiar.png"))); // NOI18N
        CleanButton.setActionCommand("Clean");
        CleanButton.setBorderPainted(false);
        CleanButton.setContentAreaFilled(false);
        add(CleanButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 200, -1, -1));

        modifyClientButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/modificar.png"))); // NOI18N
        modifyClientButton.setActionCommand("ModifyClient");
        modifyClientButton.setBorderPainted(false);
        modifyClientButton.setContentAreaFilled(false);
        add(modifyClientButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 200, 120, 40));

        ErrorLabel.setBackground(new java.awt.Color(255, 0, 0));
        ErrorLabel.setForeground(new java.awt.Color(255, 0, 0));
        add(ErrorLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 50, 150, 20));

        jLabel7.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel7.setText("Teléfono");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 170, -1, -1));

        jLabel8.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel8.setText("Correo");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 130, -1, -1));

        jLabel9.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel9.setText("Dirección");
        add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 90, -1, -1));

        jLabel11.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel11.setText("Edad");
        add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 50, -1, -1));

        jLabel6.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel6.setText("Nombre");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 130, -1, -1));

        jLabel5.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel5.setText("Apellidos");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 170, -1, -1));

        jLabel4.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel4.setText("Cedula");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 90, -1, -1));

        jLabel3.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel3.setText("ID");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 50, -1, -1));

        jLabel2.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel2.setText("Buscar");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 290, -1, 30));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/barra 3.png"))); // NOI18N
        add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 10, 870, 250));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/barra 4.png"))); // NOI18N
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 260, 870, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void ClientTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ClientTableMouseClicked
        int Selection = ClientTable.rowAtPoint(evt.getPoint());
        idTextField.setText(String.valueOf(ClientTable.getValueAt(Selection, 0)));
        cedFormattedTextField.setText(String.valueOf(ClientTable.getValueAt(Selection, 1)));
        nameTextField.setText(String.valueOf(ClientTable.getValueAt(Selection, 2)));
        lastnameTextField.setText(String.valueOf(ClientTable.getValueAt(Selection, 3)));
        ageTextField.setText(String.valueOf(ClientTable.getValueAt(Selection, 4)));
        directionTextField.setText(String.valueOf(ClientTable.getValueAt(Selection, 5)));
        mailTextField.setText(String.valueOf(ClientTable.getValueAt(Selection, 6)));
        phoneFormattedTextField.setText(String.valueOf(ClientTable.getValueAt(Selection, 7)));
    }//GEN-LAST:event_ClientTableMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton CleanButton;
    public javax.swing.JTable ClientTable;
    public javax.swing.JLabel ErrorLabel;
    private javax.swing.JButton addclientButton;
    private javax.swing.JTextField ageTextField;
    private javax.swing.JTextField cedFormattedTextField;
    private javax.swing.JButton deleteClientButton;
    private javax.swing.JTextField directionTextField;
    private javax.swing.JTextField idTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField lastnameTextField;
    private javax.swing.JTextField mailTextField;
    private javax.swing.JButton modifyClientButton;
    private javax.swing.JTextField nameTextField;
    private javax.swing.JTextField phoneFormattedTextField;
    private javax.swing.JTextField searchclientTextField;
    // End of variables declaration//GEN-END:variables
}

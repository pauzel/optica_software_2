package paneles;

import ConnectionSql.ConnectionSQLServer;
import Controllers.SalesController;
import Misc.DesignTable;
import Misc.ShowTables;
import Models.SalesModel;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.Icon;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class Sales extends javax.swing.JPanel {

    ShowTables STB = new ShowTables();
    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();

    public Sales() {
        initComponents();
        performance();
    }

    public void performance() {
        setController();
        DesignTable dt = new DesignTable();
        dt.DesignTable(salesTable);
        STB.LoadSalesTable(salesTable);
        this.searchidButton.setCursor(new Cursor(HAND_CURSOR));
        this.generatesalesButton.setCursor(new Cursor(HAND_CURSOR));
    }

    public void setController() {
        SalesController sc = new SalesController(this);
        searchcodeButton.addActionListener(sc);
        searchidButton.addActionListener(sc);
        generatesalesButton.addActionListener(sc);
    }

    public SalesModel getSalesData() {
        SalesModel sm = new SalesModel();

        sm.setID_Client(idclientTextField.getText());
        sm.setCode_Prod(codeTextField.getText());
        
        return sm;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        salesTable = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        brandTextField = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        idclientTextField = new javax.swing.JTextField();
        ClienteTextField = new javax.swing.JTextField();
        codeTextField = new javax.swing.JTextField();
        eliminarButton = new javax.swing.JButton();
        generatesalesButton = new javax.swing.JButton();
        searchcodeButton = new javax.swing.JButton();
        searchidButton = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(36, 148, 216));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        salesTable.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        salesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "ID Venta", "ID Cliente", "Cliente", "ID Producto", "Producto", "Fecha"
            }
        ));
        salesTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        jScrollPane2.setViewportView(salesTable);

        add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 120, 450, 410));

        jLabel4.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel4.setText("Producto");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 330, -1, -1));
        add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 70, 220, 30));
        add(brandTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 330, 120, -1));

        jTextField3.setEditable(false);
        add(jTextField3, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 170, 120, -1));
        add(idclientTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 210, 80, -1));
        add(ClienteTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 250, 120, -1));
        add(codeTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 290, 80, -1));

        eliminarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/eliminar.png"))); // NOI18N
        eliminarButton.setBorderPainted(false);
        eliminarButton.setContentAreaFilled(false);
        eliminarButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                eliminarButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                eliminarButtonMouseExited(evt);
            }
        });
        add(eliminarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 400, 120, 30));

        generatesalesButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/guardar.png"))); // NOI18N
        generatesalesButton.setActionCommand("GenerateSales");
        generatesalesButton.setBorderPainted(false);
        generatesalesButton.setContentAreaFilled(false);
        generatesalesButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                generatesalesButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                generatesalesButtonMouseExited(evt);
            }
        });
        add(generatesalesButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 400, 120, 30));

        searchcodeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/buscar.png"))); // NOI18N
        searchcodeButton.setActionCommand("SearchCode");
        add(searchcodeButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 290, 30, 20));

        searchidButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/buscar.png"))); // NOI18N
        searchidButton.setActionCommand("SearchID");
        add(searchidButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 210, 30, 20));

        jLabel8.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel8.setText("ID Producto");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 290, -1, -1));

        jLabel7.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel7.setText("Cliente");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 250, -1, -1));

        jLabel6.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel6.setText("ID Cliente");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 210, -1, -1));

        jLabel5.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel5.setText("ID Venta");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 170, -1, -1));

        jLabel3.setFont(new java.awt.Font("Lato", 0, 14)); // NOI18N
        jLabel3.setText("Buscar");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 70, 50, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/datosinv.png"))); // NOI18N
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 100, 330, 410));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/tablainv.png"))); // NOI18N
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 20, 560, 560));
    }// </editor-fold>//GEN-END:initComponents

    private void generatesalesButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_generatesalesButtonMouseEntered
        generatesalesButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/newResources/guardar_efecto.png")));
    }//GEN-LAST:event_generatesalesButtonMouseEntered

    private void generatesalesButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_generatesalesButtonMouseExited
        generatesalesButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/newResources/guardar.png")));
    }//GEN-LAST:event_generatesalesButtonMouseExited

    private void eliminarButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_eliminarButtonMouseEntered
        eliminarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/newResources/eliminar_efecto.png")));
    }//GEN-LAST:event_eliminarButtonMouseEntered

    private void eliminarButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_eliminarButtonMouseExited
         eliminarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/newResources/eliminar.png")));
    }//GEN-LAST:event_eliminarButtonMouseExited


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JTextField ClienteTextField;
    public javax.swing.JTextField brandTextField;
    private javax.swing.JTextField codeTextField;
    private javax.swing.JButton eliminarButton;
    private javax.swing.JButton generatesalesButton;
    private javax.swing.JTextField idclientTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField3;
    public javax.swing.JTable salesTable;
    private javax.swing.JButton searchcodeButton;
    private javax.swing.JButton searchidButton;
    // End of variables declaration//GEN-END:variables
}

package paneles;

import Controllers.SalesRecordController;
import Misc.ShowTables;
import Models.SalesRecordModel;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import javax.swing.Icon;

public class salesPanel extends javax.swing.JPanel {
    ShowTables st = new ShowTables();

    public salesPanel() {
        initComponents();
        setController();
        st.LoadTableSalesRecord(salesrecordTable);
        this.reportButton.setCursor(new Cursor(HAND_CURSOR));
    }
    
    public void setController(){
        SalesRecordController s = new SalesRecordController(this);
        reportButton.addActionListener(s);
    }
     
    public SalesRecordModel setData(){
        SalesRecordModel S = new SalesRecordModel();
        
        S.setID_Sales(idsalesTextField.getText());
        
        return S;
    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        reportButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        salesrecordTable = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        idsalesTextField = new javax.swing.JTextField();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        reportButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/reportButton.png"))); // NOI18N
        reportButton.setActionCommand("salesreport");
        reportButton.setBorderPainted(false);
        reportButton.setContentAreaFilled(false);
        reportButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                reportButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                reportButtonMouseExited(evt);
            }
        });
        add(reportButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 100, 140, 30));

        salesrecordTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "ID_Sales", "ID_Client", "Name", "LastName", "Product", "Color", "Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        salesrecordTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                salesrecordTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(salesrecordTable);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 140, 900, 440));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/viñeta_salesrecord.png"))); // NOI18N
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 960, 610));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/background_salesrecord.png"))); // NOI18N
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 960, 610));
        add(idsalesTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 110, 60, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void reportButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_reportButtonMouseEntered
        reportButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/reportButton.png")));
    }//GEN-LAST:event_reportButtonMouseEntered

    private void reportButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_reportButtonMouseExited
        reportButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/reportButtonefecto.png")));
    }//GEN-LAST:event_reportButtonMouseExited

    private void salesrecordTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salesrecordTableMouseClicked
        int Selection = salesrecordTable.rowAtPoint(evt.getPoint());
        idsalesTextField.setText(String.valueOf(salesrecordTable.getValueAt(Selection, 0)));
    }//GEN-LAST:event_salesrecordTableMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField idsalesTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton reportButton;
    private javax.swing.JTable salesrecordTable;
    // End of variables declaration//GEN-END:variables
}

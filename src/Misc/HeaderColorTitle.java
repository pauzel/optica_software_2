package Misc;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class HeaderColorTitle extends DefaultTableCellRenderer {

    public HeaderColorTitle() {
        setOpaque(true);
    }

    public Component getTableCellRendererComponent(JTable jTable, Object value, boolean selected, boolean focused, int row, int column) {
        super.getTableCellRendererComponent(jTable, value, selected, focused, row, column);
        setBackground(Color.DARK_GRAY);
        setForeground(Color.WHITE);
        return this;
    }
}

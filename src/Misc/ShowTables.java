package Misc;

import ConnectionSql.ConnectionSQLServer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.sql.*;

public class ShowTables {

    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();
    DesignTable DT = new DesignTable();
    
    public void InventoryTable(JTable tabla){
        DT.DesignTable(tabla);
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);
        
        try {
            CallableStatement cs = con.prepareCall("SELECT * FROM Stock");
            ResultSet rs = cs.executeQuery();
            
            while(rs.next()){
                Vector v = new Vector();
                v.add(rs.getString(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                model.addRow(v);
            }
        } catch (Exception e) {
            System.out.println();
        }
    }

    public void OptometrySearchTable(JTable tabla, String code) {
        DT.DesignTable(tabla);
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "EXEC OptometrySearch '" + code + "'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                v.add(rs.getString(7));
                v.add(rs.getString(8));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }

    }

    public void LoadOptometryTable(JTable tabla) {
        DT.DesignTable(tabla);
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM dbo.Optometry";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                v.add(rs.getString(7));
                v.add(rs.getString(8));
                v.add(rs.getString(9));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }

    }

    public void SalesSearchTable(JTable tabla, String code) {
        DT.DesignTable(tabla);
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "EXEC SalesSearch '" + code + "'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(7));
                v.add(rs.getString(8));
                v.add(rs.getString(9));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }

    }

    public void LoadSalesTable(JTable tabla) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM dbo.SalesF";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getInt(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }

    }

    public void ClientSearchTable(JTable tabla, String ced) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "EXEC ClientSearch '" + ced + "'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                v.add(rs.getString(7));
                v.add(rs.getString(8));
                model.addRow(v);
                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void LoadClientTable(JTable tabla) {
        DT.DesignTable(tabla);
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM ViewClients";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                v.add(rs.getString(7));
                v.add(rs.getString(8));
                v.add(rs.getString(9));
                model.addRow(v);
                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }

    }

    public void SearchInventory(JTable tabla, String code) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "EXEC InventorySearch '" + code + "'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                model.addRow(v);
                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void SearchUserTable(JTable tabla, String ced) {
        DT.DesignTable(tabla);
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "EXEC UserSearch '" + ced + "'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                v.add(rs.getString(7));
                v.add(rs.getString(8));
                v.add(rs.getString(9));
                v.add(rs.getString(10));
                v.add(rs.getString(11));
                v.add(rs.getString(12));
                v.add(rs.getString(13));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void LoadUserTable(JTable tabla) {
        DT.DesignTable(tabla);
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM ViewUsers";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                v.add(rs.getString(7));
                v.add(rs.getString(8));
                v.add(rs.getString(9));
                v.add(rs.getString(10));
                v.add(rs.getString(11));
                v.add(rs.getString(12));
                v.add(rs.getString(13));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }

    }

    public void LoadTableSalesRecord(JTable tabla) {
        DT.DesignTable(tabla);
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM SalesRecord";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                v.add(rs.getString(7));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }

    }
}

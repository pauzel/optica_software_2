/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controllers.LoginControllers;
import Models.LoginModel;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author pc
 */
public class LoginForm extends javax.swing.JFrame {
    LoginControllers LC;
    /**
     * Creates new form Login
     */
    public LoginForm() {
        initComponents();
        this.setLocationRelativeTo(this);
        this.exitButton.setCursor(new Cursor(HAND_CURSOR));
        this.LoginButton.setCursor(new Cursor(HAND_CURSOR));
        this.forgetpassButton.setCursor(new Cursor(HAND_CURSOR));
        this.viewpassCheckBox.setCursor(new Cursor(HAND_CURSOR));
        //setIconImage(new ImageIcon(getClass().getResource("/Resources/image/icono_barra.png")).getImage());
        setController();

    }

    public void Clean() {
        userTextField.setText("");
        passwordTextField.setText("");
    }

    private void setController() {
        LC = new LoginControllers(this);
        LoginButton.addActionListener(LC);
    }

    public LoginModel GetData() {
        LoginModel LM = new LoginModel();
        LM.setUser(userTextField.getText());
        LM.setPassword(String.valueOf(passwordTextField.getPassword()));
        return LM;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        error = new javax.swing.JLabel();
        viewpassCheckBox = new javax.swing.JCheckBox();
        forgetpassButton = new javax.swing.JButton();
        LoginButton = new javax.swing.JButton();
        exitButton = new javax.swing.JButton();
        passwordTextField = new javax.swing.JPasswordField();
        userTextField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        error.setForeground(new java.awt.Color(255, 51, 51));
        getContentPane().add(error, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 150, 130, 20));

        viewpassCheckBox.setToolTipText("");
        viewpassCheckBox.setContentAreaFilled(false);
        viewpassCheckBox.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/viewpass.png"))); // NOI18N
        viewpassCheckBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                viewpassCheckBoxMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                viewpassCheckBoxMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                viewpassCheckBoxMouseExited(evt);
            }
        });
        getContentPane().add(viewpassCheckBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(567, 231, -1, 30));

        forgetpassButton.setFont(new java.awt.Font("Arial", 1, 10)); // NOI18N
        forgetpassButton.setForeground(new java.awt.Color(0, 25, 112));
        forgetpassButton.setText("Forget Password?");
        forgetpassButton.setBorderPainted(false);
        forgetpassButton.setContentAreaFilled(false);
        forgetpassButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                forgetpassButtonActionPerformed(evt);
            }
        });
        getContentPane().add(forgetpassButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 320, 140, -1));

        LoginButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/login.png"))); // NOI18N
        LoginButton.setActionCommand("Login");
        LoginButton.setBorderPainted(false);
        LoginButton.setContentAreaFilled(false);
        LoginButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LoginButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LoginButtonMouseExited(evt);
            }
        });
        getContentPane().add(LoginButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 290, 140, 30));

        exitButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/exit.png"))); // NOI18N
        exitButton.setBorder(null);
        exitButton.setBorderPainted(false);
        exitButton.setContentAreaFilled(false);
        exitButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                exitButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                exitButtonMouseExited(evt);
            }
        });
        exitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitButtonActionPerformed(evt);
            }
        });
        getContentPane().add(exitButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 10, 30, -1));

        passwordTextField.setBackground(new java.awt.Color(48, 63, 159));
        passwordTextField.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        passwordTextField.setForeground(new java.awt.Color(255, 255, 255));
        passwordTextField.setBorder(null);
        getContentPane().add(passwordTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 230, 110, 30));

        userTextField.setBackground(new java.awt.Color(48, 63, 159));
        userTextField.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        userTextField.setForeground(new java.awt.Color(255, 255, 255));
        userTextField.setBorder(null);
        userTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userTextFieldActionPerformed(evt);
            }
        });
        getContentPane().add(userTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 180, 110, 30));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/Login.jpg"))); // NOI18N
        jLabel2.setPreferredSize(new java.awt.Dimension(634, 421));
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 634, 421));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitButtonMouseEntered
        exitButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/exit_efecto.png")));
    }//GEN-LAST:event_exitButtonMouseEntered

    private void exitButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitButtonMouseExited
        exitButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/exit.png")));
    }//GEN-LAST:event_exitButtonMouseExited

    private void exitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitButtonActionPerformed
        dispose();
    }//GEN-LAST:event_exitButtonActionPerformed

    private void LoginButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LoginButtonMouseEntered
        LoginButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/login_efecto.png")));
    }//GEN-LAST:event_LoginButtonMouseEntered

    private void LoginButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LoginButtonMouseExited
        LoginButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/newResources/login.png")));
    }//GEN-LAST:event_LoginButtonMouseExited

    private void viewpassCheckBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_viewpassCheckBoxMouseClicked
        if (viewpassCheckBox.isSelected()) {
            //CAPTURA LO ESCRITO Y LO MUESTA TIPO STRING
            viewpassCheckBox.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/newResources/viewpass2.png")));
            this.passwordTextField.setEchoChar((char) 0);
        } else {
            //CAPTURA LO ESCRITO Y LO MUESTR CON 
            viewpassCheckBox.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/newResources/viewpass.png")));
            this.passwordTextField.setEchoChar('*');
        }
    }//GEN-LAST:event_viewpassCheckBoxMouseClicked

    private void viewpassCheckBoxMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_viewpassCheckBoxMouseEntered
        viewpassCheckBox.setToolTipText("show password / hide password ");
    }//GEN-LAST:event_viewpassCheckBoxMouseEntered

    private void viewpassCheckBoxMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_viewpassCheckBoxMouseExited
        viewpassCheckBox.setToolTipText("show password / hide password ");
    }//GEN-LAST:event_viewpassCheckBoxMouseExited

    private void forgetpassButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_forgetpassButtonActionPerformed
        RecoveryPassword Recovery = new RecoveryPassword();
        Recovery.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_forgetpassButtonActionPerformed

    private void userTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_userTextFieldActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoginForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton LoginButton;
    public javax.swing.JLabel error;
    private javax.swing.JButton exitButton;
    private javax.swing.JButton forgetpassButton;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPasswordField passwordTextField;
    private javax.swing.JTextField userTextField;
    private javax.swing.JCheckBox viewpassCheckBox;
    // End of variables declaration//GEN-END:variables
}
